<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('fire', 'AdminController@fired');

Route::post('event/sent', function(){ event(new App\Events\Notice()); });


Route::get('admin/login', 'AdminController@getLogin');
Route::post('admin/login', 'AdminController@postLogin');
Route::get('user/login', 'UserController@getLogin');
Route::post('user/login', 'UserController@postLogin');
Route::get('user/logout', 'UserController@logout');

Route::group(['middleware' => 'authAdmin:admin'], function(){
	Route::get('admin/index', 'AdminController@index');
	Route::get('category/index', 'CategoryController@index');
	Route::get('content/index', 'ContentController@index');
	Route::get('comment/index', 'CommentController@index');
	Route::get('tag/index', 'TagController@index');
	
	Route::resource('category', 'CategoryController');
	Route::resource('comment', 'CommentController');
	Route::resource('content', 'ContentController');
	Route::resource('tag', 'TagController');
	
	Route::get('admin/settings', 'AdminController@getSettings');
	Route::post('admin/settings', 'AdminController@postSettings');
});

Route::group(['middleware' => 'authUser:user'], function(){
	Route::resource('user', 'UserController');
});
Auth::routes();

Route::get('/home', 'HomeController@index');

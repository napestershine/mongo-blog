<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class Content extends Model
{
	use HybridRelations;
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    
    /**
     *
     *
     *      
     */
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'cid');
    }
    
    /**
     * 
     * 
     * 
     */
    public function tags()
    {
        return $this->hasMany('App\Tag', 'cid', 'id')->where('type', 'content');
    }
}

<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Model implements Authenticatable
{

    use AuthenticableTrait;
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';


    protected $fillable = [
        'name', 'email', 'password', 'role', 'status'
    ];
}

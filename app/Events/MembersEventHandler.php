<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MembersEventHandler implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;
	
	public $member;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
	
	public function add(ChatMember $member)
    {
	    $this->member = $member;
	    $this->member->type = "add";
    }
	
	public function update(ChatMember $member)
    {
	    $this->member = $member;
	    $this->member->type = "update";
    }
    
    public function delete($id)
    {
	    $this->member->id = $id;
	    $this->member->type = "delete";
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['chat-members'];
    }
}

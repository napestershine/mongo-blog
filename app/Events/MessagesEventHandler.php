<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessagesEventHandler implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;
	
	public $message;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function add(ChatMessage $member)
    {
	    $this->message = $member;
	    $this->message->type = "add";
    }
    
    public function delete($id)
    {
	    $this->message->id = $id;
	    $this->message->type = "delete";
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
    	$hash = "msg-" . uniqHash();
        return [$hash];
    }
}

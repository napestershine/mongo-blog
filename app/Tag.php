<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Tag extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    protected $collection = 'tags';

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $guarded = ['_id'];

    /**
     *
     *
     */
    public function content()
    {
        return $this->belongsTo('App\Content', 'cid', 'id');
    }
}

<?php

function iif($value, $first, $second)
{
	return !is_empty($value)? $first : $second;
}

function settings($key = NULL)
{
	$value = Cache::rememberForever('settings', function()
    {
        return App\Setting::pluck('value', 'name');
    });
	
	if(!is_null($key)){
		$value = $value[$key];
	}
	
    return $value;
}

function uniqHash()
{
	return md5(Request::ip() . Request::server('HTTP_USER_AGENT'));
}
?>
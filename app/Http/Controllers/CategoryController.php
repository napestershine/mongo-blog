<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;

use App\Http\Controllers\Controller;

use Validator;

use Session;

class CategoryController extends Controller
{
	/**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        
        return view('category.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $category = new Category;
        $category->name = $request->name;
        $category->seoname = $request->seoname;
        $category->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5|max:100',
            'seoname' => 'required|unique:categories,seoname|min:5|max:100'
        ]);

        if ($validator->fails()) {
            return redirect('category/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $category->save();
        
        Session::flash('success', 'Category has been added!');
        
        return redirect('category/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        
        return view('category.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $category = Category::find($id);
        $category->name = $request->name;
        $category->seoname = $request->seoname;
        $category->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5|max:100',
            'seoname' => 'required|min:5|max:100'
        ]);

        if ($validator->fails()) {
            return redirect('category/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $category->save();
        
        
        Session::flash('success', 'Category has been updated!');
        
        return redirect('category/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        
        Session::flash('danger', 'Category has been deleted!');
        
        return redirect('category/index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Tag;

use App\Http\Controllers\Controller;

class TagController extends Controller
{
	/**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('tag.lists');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $tag = new Tag;
        $tag->cid = $request->cid;
        $tag->tag = $request->tag;
        $tag->seotag = $request->seotag;
        $tag->type = $request->type;
        $tag->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'tag' => 'required|min:2|max:50',
            'seotag' => 'required|unique:tags,seotag|min:2|max:50'
        ]);

        if ($validator->fails()) {
            return redirect('tag/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $tag->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tag = App\Tag::find($id);
        
        return view('tag.edit', $tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tag = App\Tag::find($id);
        //$tag->cid = $request->cid;
        $tag->tag = $request->tag;
        $tag->seotag = $request->seotag;
        //$tag->type = $request->type;
        $tag->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'tag' => 'required|min:2|max:50',
            'seotag' => 'required|unique:tags,seotag|min:2|max:50'
        ]);

        if ($validator->fails()) {
            return redirect('tag/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $tag->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        App\Tag::destroy($id);
    }
}

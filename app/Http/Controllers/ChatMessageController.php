<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChatMessageController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
    	
    }
    
    public function show($cid)
    {
	    $messages = App\ChatMessage::where('cid', $cid)->toJson();
    	
    	return $messages;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        //
        $message = new ChatMessage;
        $message->cid = $request->cid;
        $message->uid = $request->uid;
        $message->ip = $request->ip;
        $message->body = $request->body;
        $message->created_at = $request->created_at;
        $message->status = 1;
        
        $message->save();
        
        Event::fire(new App\Events\MessagesEventHandler()->add($message));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = App\ChatMessage::find($id);
        $message->status = 0;
        
        $message->save();
        
        Event::fire(new App\Events\MessagesEventHandler()->delete($id));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Http\Controllers\Controller;

class UserController extends Controller
{

	/**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('user.lists');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = new User;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->authlevel = $request->authlevel;
        $user->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users,username|min:5|max:100',
        	'email' => 'required|unique:users,email|email|max:100',
        	'password' => 'required_if:password_again|min:6|max:16',
        	'authlevel' => 'required|integer|max:1'
        ]);

        if ($validator->fails()) {
            return redirect('user/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        
        return view('user.edit', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);
        
        $user->username = $request->username;
        if(isset($request->password))
        {
	        $user->password = bcrypt($request->password);
        }
        if(isset($request->email))
        {
	        $user->email = $request->email;
        }
        $user->authlevel = $request->authlevel;
        $user->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'username' => 'required|min:5|max:100',
        	'email' => 'required|email|max:100',
        	'password' => 'required_if:password_again|min:6|max:16',
        	'authlevel' => 'required|integer|max:1'
        ]);

        if ($validator->fails()) {
            return redirect('user/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::destroy($id);
    }
    
    public function getLogin()
	{
	    return view('user.login');
	}
	
    public function postLogin(Request $request)
    {
	    $user = User::where('email', $request->email)->where('role', 'user')->where('status', 1)->first();
		
	    if($user->count() > 0 && Hash::check($request->password, $user->password))
	    {
		    $request->session()->put('uid', $user->id);
		    $request->session()->put('role', $user->role);
		    
		    return redirect('user/index');
		    
	    }
	    else
	    {
		   // some error handling here.
	    }

    }
    
    public function logout(Request $request)
    {
	    $request->session()->flush();
	    
	    return redirect(url()->previous());
    }
}

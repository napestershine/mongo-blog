<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChatMemberController extends Controller
{
	private $timeout = 600; // 10 Dk.
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
    	
    }
    
    public function show()
    {
	    $messages = App\ChatMember::where('created_at', '<', time() - $this->timeout)->toJson();
    	
    	return $messages;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        //
        $member = new ChatMember;
        $member->cid = uniqHash();
        $member->ip = $request->ip;
        $member->city = $request->city;
        $member->referance = $request->referance;
        $member->location = $request->location;
        $member->created_at = $request->created_at;
        $member->status = 1;
        
        $member->save();
        
        Event::fire(new App\Events\MembersEventHandler()->add($member));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($request, $id)
    {
        //
        $member = App\ChatMember::find($id);
        $member->location = $request->location;
        $member->created_at = $request->created_at;
        $member->status = $request->status;
        
        $member->save();
        
        Event::fire(new App\Events\MembersEventHandler()->update($member));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = App\ChatMember::find($id);
        $member->status = 0;
        
        $member->save();
        
        Event::fire(new App\Events\MembersEventHandler()->delete($id));
    }
}

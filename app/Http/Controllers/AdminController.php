<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Setting;

use Illuminate\Support\Facades\Hash;

use Validator;

use Session;

use Illuminate\Support\Facades\Redis;

class AdminController extends Controller
{
	/**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
    	
    }
    
    public function fired(){
	    //$redis = Redis::connection();
	    $data['channel'] = 'c_' . uniqHash();
		$data['data'] = ['message' => 'Test', 'user' => uniqHash()];
		//$redis->publish('notifications', json_encode($data));
		Redis::publish('chat', json_encode($data));
		echo "tamam";
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }
	
	public function getLogin()
	{
	    return view('user.login');
	}
	
    public function postLogin(Request $request)
    {

	    $user = User::where('email', $request->email)->where('role', 'admin')->where('status', 1);
	    if($user->count() > 0)
	    {
	    	$user = $user->firstOrFail();

	    	if(Hash::check($request->password, $user->password)){
		    	$request->session()->put('uid', $user->id);
		    	$request->session()->put('role', $user->role);
		    }
		    return redirect('admin/index');
		    
	    }
	    else
	    {
		   Session::flash('error', 'Hata Mevcut');
	    }
	    
	    return redirect('admin/login');
    }
    
    public function getSettings()
    {
	    $settings = settings();
	    
	    return $settings;
    }
    
    public function postSettings(Request $request)
    {
    	Setting::truncate();
    	Cache::forget('settings');
    	
    	foreach($request->all() as $name => $value)
    	{
	    	$settings = new Setting;
		    $settings->name = $name;
		    $settings->value = $value;
		    $settings->save();
    	}
	    
	    return "Settings has been updated!";
    }
}

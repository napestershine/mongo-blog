<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Comment;

use App\Http\Controllers\Controller;

class CommentController extends Controller
{
	/**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('comment.lists');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('comment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $comment = new Comment;
        $comment->cid = $request->cid;
        $comment->uid = $request->uid; // sess den al
        $comment->comment = $request->comment;
        $comment->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'comment' => 'required|min:3'
        ]);

        if ($validator->fails()) {
            return redirect('comment/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $comment->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $comment = App\Comment::find($id);
        
        return view('comment.edit', $comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $comment = App\Comment::find($id);
        //$comment->cid = $request->cid;
        $comment->comment = $request->comment;
        $comment->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'comment' => 'required|min:3'
        ]);

        if ($validator->fails()) {
            return redirect('comment/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $comment->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        App\Comment::destroy($id);
    }
}

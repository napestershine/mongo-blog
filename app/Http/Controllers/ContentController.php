<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Content;

use App\Category;

use App\Tag;

use App\Http\Controllers\Controller;

use Validator;

use Session;

class ContentController extends Controller
{
	/**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = Content::with('category')->with('tags')->get();
        
        return view('content.index', ['contents' => $contents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        
        return view('content.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $content = new Content;
        $content->cid = $request->category;
        $content->uid = $request->session()->get('uid');
        $content->title = $request->title;
        $content->seotitle = $request->seotitle;
        $content->content = $request->content;
        $content->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|max:100',
            'seotitle' => 'required|unique:contents,seotitle|min:3|max:100',
            'content' => 'required|min:10'
        ]);

        if ($validator->fails()) {
            return redirect('content/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $content->save();
        
        foreach($request->tags as $tag){
	        $tags = new Tag;
	        $tags->cid = $content->id;
	        $tags->name = $tag;
	        $tags->seoname = $tag;
	        $tags->type = 'content';
	        
	        $tags->save();
        }
        
        Session::flash('success', 'Content has been added!');
        
        return redirect('content/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $content = Content::with('category')->with('tags')->find($id);
        $categories = Category::all();
        
        return view('content.edit', ['content' => $content, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $content = Content::find($id);
        $content->cid = $request->category;
        $content->title = $request->title;
        $content->seotitle = $request->seotitle;
        $content->content = $request->content;
        $content->status = $request->status;
        
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|max:100',
            'seotitle' => 'required|min:3|max:100',
            'content' => 'required|min:10'
        ]);

        if ($validator->fails()) {
            return redirect('content/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $content->save();
        
        Tag::where('cid', (int)$content->id)->where('type', 'content')->delete();
        
        foreach($request->tags as $tag){
	        $tags = new Tag;
	        $tags->cid = $content->id;
	        $tags->name = $tag;
	        $tags->seoname = $tag;
	        $tags->type = 'content';
	        
	        $tags->save();
        }
        
        Session::flash('success', 'Content has been updated!');
        
        return redirect('content/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //Content::destroy($id);
        Tag::where('cid', (int)$id)->where('type','content')->delete();
        Session::flash('danger', 'Content has been deleted!');
        
        return redirect('content/index');
    }
}

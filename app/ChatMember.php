<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class ChatMember extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    protected $collection = 'chat_members';
}

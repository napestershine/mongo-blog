<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Category extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    
    /**
     *
     *      
     */
    public function content()
    {
        return $this->belongsTo('App\Content', 'cid', 'id');
    }
}

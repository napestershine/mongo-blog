<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class ChatMessage extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';
    protected $collection = 'chat_messages';
}

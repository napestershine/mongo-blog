var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function(socket){
  console.log('a user connected');
  console.log('Total user: ' + io.engine.clientsCount);
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  
  socket.on('chat', function(data){
    console.log('message: ' + data.channel);
    //io.emit('chat', msg);
    io.emit(data.channel, data.msg);
  });
  
});
/*
var Redis = require('ioredis');
var redis = new Redis();
redis.subscribe('chat', function(err, count) {
});
redis.on('message', function(channel, message) {
    console.log('Message Recieved: ' + message);
    message = JSON.parse(message);
    //io.emit(channel, message.data);
    
	//Send this event to everyone in the room.
	io.emit(message.channel, message.data);
    
});
*/
http.listen(3000, function(){
    console.log('Listening on Port 3000');
});

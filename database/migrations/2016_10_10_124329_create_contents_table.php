<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('cid');
            $table->integer('uid');
            $table->string('title', 255);
            $table->string('seotitle', 255)->unique();
            $table->mediumText('content');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            
            $table->index(['seotitle', 'status']);
            $table->index('cid');
            $table->index('uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}

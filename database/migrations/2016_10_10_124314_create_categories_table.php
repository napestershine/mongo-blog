<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
        	$table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('name', 100);
            $table->string('seoname', 100)->unique();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            
            $table->index(['seoname', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

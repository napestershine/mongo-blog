<?php

use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('tags', function(Blueprint $collection)
		{
			$collection->index('cid');
			$collection->index('type');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('tags', function(Blueprint $collection)
		{
			$collection->dropIndex('cid');
			$collection->dropIndex('type');
		});
    }
}

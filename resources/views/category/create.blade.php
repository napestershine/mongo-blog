@extends('admin.root')



@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="/admin/index">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/category/index">Category Add</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Category Management </h1>
    <!-- END PAGE TITLE-->
	<div class="row">
		<div class="col-md-8">
		    <!-- BEGIN EXAMPLE TABLE PORTLET-->
		    <div class="portlet light bordered">
		    	@if (Session::has('success'))
					<div class="alert alert-success alert-dismissable"><strong></strong> {{Session::get('success')}} </div>
				@endif
				@foreach ($errors->all() as $error)
					<div class="alert alert-danger alert-dismissable"><strong></strong> {{$error}} </div>
				@endforeach
		        <div class="portlet-title">
		            <div class="caption font-dark">
		                <i class="icon-settings font-dark"></i>
		                <span class="caption-subject bold uppercase">Category Add</span>
		            </div>
		            <div class="tools"> </div>
		        </div>
		        <div class="portlet-body form">
		            <form action="/category" method="POST" role="form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-body">
							<div class="form-group">
                                <label>Category Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-xlarge" placeholder="Category Name" name="name"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Category Seo Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-xlarge" placeholder="Category Seo Name" name="seoname"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Category Status</label>
                                <div class="input-group">
                                	<select name="status" class="form-control input-xlarge">
                                		<option value="0">Passive</option>
                                		<option value="1" selected="selected">Active</option>
                                	</select>
                                </div>
                            </div>
                            <div class="form-actions right">
                                <button type="submit" class="btn green">Create</button>
                            </div>
						</div>
		            </form>
		        </div>
		    </div>
		    <!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
    </div>
<!-- END CONTENT BODY -->
@endsection

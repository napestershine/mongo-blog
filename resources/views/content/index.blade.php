@extends('admin.root')

@section('headerjs')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet"
          type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="/admin/index">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="/category/index">Content List</a>
                    <i class="fa fa-circle"></i>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Content Management </h1>
        <!-- END PAGE TITLE-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    @if (Session::has('danger'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <strong></strong> {{Session::get('danger')}}
                        </div>
                    @endif
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Contents</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Tags</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <td>{{$content->_id}}</td>
                                    <td><a href="/{{$content->category->seoname}}/{{$content->seotitle}}.html"
                                           target="_blank"> {{$content->title}} </a></td>
                                    <td><a href="/category/{{$content->category->seoname}}.html"
                                           target="_blank"> {{$content->category->name}} </a></td>
                                    <td>
                                        @foreach($content->tags as $tag)
                                            <a href="/tag/{{$tag->seoname}}.html" target="_blank"> <span
                                                        class="label label-success">{{$tag->name}}</span> </a>
                                        @endforeach
                                    </td>
                                    <td>
		                        	<span class="label @if ($content->status === 1) label-success @else label-danger @endif"> 
			                        @if ($content->status === 1)
                                            Active
                                        @else
                                            Passive
                                        @endif
			                        </span>
                                    </td>
                                    <td>
		                        	<span style="float:left;">
			                        <form id="act_{{$content->id}}"
                                          onsubmit="if(!confirm('Are you sure to want to delete this content?')) return false;"
                                          action="/content/{{$content->id}}" method="POST">
										<input type="hidden" name="_method" value="DELETE"/>
										<input type="hidden" name="id" value="{{$content->id}}"/>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<button type="submit" class="btn sbold red-sunglo">
			                            	<i class="fa fa-times"></i>
			                            	DELETE
										</button>
									</form>
									</span>
                                        <span>
									<a href="/content/{{$content->id}}/edit" class="btn sbold blue">
		                            	<i class="fa fa-edit"></i>
		                            	EDIT
									</a>
									</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
@endsection

@section('footerjs')
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
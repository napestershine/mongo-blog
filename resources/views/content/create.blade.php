@extends('admin.root')

@section('headerjs')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="../assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="/admin/index">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="/category/index">Content Add</a>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Content Management </h1>
    <!-- END PAGE TITLE-->
	<div class="row">
		<div class="col-md-12">
		    <!-- BEGIN EXAMPLE TABLE PORTLET-->
		    <div class="portlet light bordered">
		    	@if (Session::has('success'))
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
						<strong></strong> {{Session::get('success')}} 
					</div>
				@endif
				@foreach ($errors->all() as $error)
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
						<strong></strong> {{$error}} 
					</div>
				@endforeach
		        <div class="portlet-title">
		            <div class="caption font-dark">
		                <i class="icon-settings font-dark"></i>
		                <span class="caption-subject bold uppercase">Content Add</span>
		            </div>
		            <div class="tools"> </div>
		        </div>
		        <div class="portlet-body form">
		            <form action="/content" method="POST" role="form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-body">
							<div class="form-group">
                                <label>Content Title</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-xlarge" placeholder="Content Title" name="title"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Content Seo Title</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-xlarge" placeholder="Content Seo Title" name="seotitle"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Content Category</label>
                                <div class="input-group">
                                    <select name="category" class="form-control input-xlarge">
                                    	<option value="">Seçiniz</option>
                                    	@foreach($categories as $category)
                                    		<option value="{{$category->id}}">{{$category->name}}</option>
                                    	@endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Content Text</label>
                                <div class="input-group">
                                	<textarea class="ckeditor form-control" name="content" rows="6"></textarea>
                                    <!-- <div name="content" id="summernote_1"> </div> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Content Tags</label>
                                <div class="input-group col-md-6">
                                    <select multiple name="tags[]" class="input-xlarge" data-role="tagsinput">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Content Status</label>
                                <div class="input-group">
                                	<select name="status" class="form-control input-xlarge">
                                		<option value="0">Passive</option>
                                		<option value="1" selected="selected">Active</option>
                                	</select>
                                </div>
                            </div>
                            <div class="form-actions right">
                                <button type="submit" class="btn green">Create</button>
                            </div>
						</div>
		            </form>
		        </div>
		    </div>
		    <!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
    </div>
<!-- END CONTENT BODY -->
@endsection

@section('footerjs')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

<script src="../assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/components-form-tools.min.js" type="text/javascript"></script>
        
<script src="../assets/pages/scripts/components-bootstrap-tagsinput.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
@endsection
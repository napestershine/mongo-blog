@section('leftsidebar')
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="icon-login"></i>
</a>
<div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
    <div class="page-quick-sidebar">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> Settings
                    <span class="badge badge-danger"></span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active page-quick-sidebar-settings" id="quick_sidebar_tab_1">
            	<form id="settings" onsubmit="$.post(this.action, $('#settings').serialize(),function(data){ toastr['success']('', data); }); return(false);" action="/admin/settings" method="POST">
                <div class="page-quick-sidebar-settings-list">
                    <h3 class="list-heading">General Settings</h3>
                    <ul class="list-items borderless">
                        <li> Comment Setting
                            <input type="checkbox" name="comment" class="make-switch" checked="checked" data-size="small" data-on-color="success" data-on-text="ON" data-off-color="danger" data-off-text="OFF" value="1" /> </li>
                        <li> Chat Setting
                            <input type="checkbox" name="chat" class="make-switch" checked="checked" data-size="small" data-on-color="success" data-on-text="ON" data-off-color="danger" data-off-text="OFF" value="1" /> </li>
                    </ul>
					<h3 class="list-heading">System Settings</h3>
                    <ul class="list-items borderless">
                        <li> Title
                            <input type="text" name="title" class="form-control input-small" placeholder="Title" value=""> 
                        </li>
                        <li> Description
                            <input type="text" name="description" class="form-control input-small" placeholder="Description" value=""> 
                        </li>
                        <li> Keyword
                            <input type="text" name="keyword" class="form-control input-small" placeholder="Keyword" value=""> 
                        </li>
                    </ul>
                    <h3 class="list-heading">Mail Settings</h3>
                    <ul class="list-items borderless">
                    	<li> SMTP Address
                            <input type="text" name="smtp_address" class="form-control input-small" placeholder="SMTP Address" value=""> 
                        </li>
                        <li> SMTP Port
                            <input type="text" name="smtp_port" class="form-control input-small" placeholder="SMTP Port" value=""> 
                        </li>
                        <li> SMTP User
                            <input type="text" name="smtp_user" class="form-control input-small" placeholder="SMTP User" value=""> 
                        </li>
                        <li> SMTP Password
                            <input type="text" name="smtp_password" class="form-control input-small" placeholder="SMTP Password" value=""> 
                        </li>
                    </ul>
                    <div class="inner-content">
                        <button type="submit" class="btn btn-success">
                            <i class="icon-settings"></i> Save Changes</button>
                    </div>
                </div>
            	</form>
            </div>
        </div>
        <script type="text/javascript">
        	$(function () {
			    $.getJSON("/admin/settings", function(data){
			    	// General Settings
				    $('input[name=comment]').bootstrapSwitch('state', !!data.comment);
				    $('input[name=chat]').bootstrapSwitch('state', !!data.chat);
				    
				    // System Settings
				    $('input[name=title]').attr('value', data.title);
				    $('input[name=description]').attr('value', data.description);
				    $('input[name=keyword]').attr('value', data.keyword);
				    
				    // Mail Settings
				    $('input[name=smtp_address]').attr('value', data.smtp_address);
				    $('input[name=smtp_port]').attr('value', data.smtp_port);
				    $('input[name=smtp_user]').attr('value', data.smtp_user);
				    $('input[name=smtp_password]').attr('value', data.smtp_password);
			    });
			});
        </script>
    </div>
</div>
<!-- END QUICK SIDEBAR -->
@endsection